# STEPS TO SETUP AND EXECUTE THE PROJECT

- Install the requirements through the requirements.txt file
- Make migrations file(s) -> python manage.py makemigrations
- Install generated migrations -> python manage.py migrate
- Command to generate proto file as per the models (schema) yhat we have defined -> python manage.py generateproto --model user.models.User --fields email,password --file user.proto
- Command to generate the corresponding gRPC code -> python -m grpc_tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ ./user.proto
- Command to run the server on local -> python manage.py grpcrunserver --dev
