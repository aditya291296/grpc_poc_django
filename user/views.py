import grpc
from .models import User
from django_grpc_framework.services import Service
from user.serializers import UserProtoSerializer
from google.protobuf import empty_pb2
from django_grpc_framework.services import Service


class UserService(Service):
    """
    GRPC service for CRUD operations on User model
    """

    def List(self, request, context):
        users = User.objects.all()
        serialized = UserProtoSerializer(users, many=True)
        for message in serialized.message:
            yield message

    def Create(self, request, context):
        serialized = UserProtoSerializer(message=request)
        serialized.is_valid(raise_exception=True)
        serialized.save()
        return serialized.message

    def get_user(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            self.context.abort(grpc.StatusCode.NOT_FOUND, "User not found")

    def Retrieve(self, request, context):
        user = self.get_user(request.id)
        serialized = UserProtoSerializer(user)
        return serialized.message

    def Update(self, request, context):
        user = self.get_user(request.id)
        serialized = UserProtoSerializer(user, message=request)
        serialized.is_valid(raise_exception=True)
        serialized.save()
        return serialized.message

    def Destroy(self, request, context):
        user = self.get_user(request.id)
        user.delete()
        return empty_pb2.Empty()