from django.db import models

# Create your models here.


class User(models.Model):
    email = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=300)
    insertedAt = models.DateTimeField(auto_now=True, db_column="inserted_at")
    updatedAt = models.DateTimeField(auto_now=True, db_column="updated_at")

    class Meta:
        db_table = "users"

    def __str__(self):
        return self.email